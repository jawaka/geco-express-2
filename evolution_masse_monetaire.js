masse_monetaire = []

function update_mm() {
    nb_credits = 0
    for (let p of players) {
        nb_credits += p.credits.length
    }
    masse_monetaire.push(nb_credits)
}


function make_evolution_mm() {
    var ctx = document.getElementById('evolution_masse_monetaire').getContext('2d');

    if (playing_md) {
        update_mm()
    }
   

    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: Array.from(Array(masse_monetaire.length).keys()),
          datasets: [{ 
              data: masse_monetaire,
              label: "Masse monétaire",
              borderColor: "#3e95cd",
              pointRadius: 0,
              fill: false
            }
          ]
        },
        options: {
            animation: false,
            
          }
    });
}
